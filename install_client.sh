link="https://gitlab.com/MaxPow2718r/codigos_servo/-/raw/main/val_client.py"
directory=$HOME/.local/bin

mkdir -p $directory

echo "Installing system required dependencies"
sudo apt install python3

echo "Downloading script"
wget -q -O "${directory}/serval" $link

chmod +x "${directory}/serval"
