# Instalar librerias
# sudo apt-get install python3-rpi.gpio
# sudo pip3 install gpiozero

from gpiozero import AngularServo
from time import sleep

# Asignar pines GPIO
Vservo = AngularServo(12, min_angle=-25, max_angle=25)
Hservo = AngularServo(13, min_angle=-25, max_angle=25)

# Valores de inicio
before_angleV_val = 0
before_angleH_val = 0

try:
    while True:
        # Pedir el angulo de cada servo (vertical y horizontal)
        angleV_val = input("Ingrese ángulo vertical: ")
        angleH_val = input("Ingrese ángulo horizonal: ")
        
        # Verfificar valor del angulo vertical
        try:
            angleV_val = float(angleV_val)
        except ValueError:
            angleV_val =  before_angleV_val
            
        # Verfificar valor del angulo horizontal
        try:
            angleH_val = float(angleH_val)
        except ValueError:
            angleH_val =  before_angleH_val
            
        # Asignar el valor de angulo a cada servo
        Vservo.angle = angleV_val
        Hservo.angle = angleH_val
        
        # Tiempo de espera
        sleep(0.5)
        
        # Guardar valores actuales
        before_angleV_val = angleV_val
        before_angleH_val = angleV_val
        
except KeyboardInterrupt:
	print("Programa finalizado")