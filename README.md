# Codigos_servo

## adjvalue
Ajusta los valores de los servos en base a valores mostrados en pantalla y
ajustados con las flechas del teclado.

Instalar las dependencias:

```sh
sudo apt install pigpio python-pigpio python3-pigpio
```

Iniciar el servidor GPIO

```sh
sudo systemctl start pigpiod
```

Iniciar el servidor automaticamente al inicio

```sh
sudo systemctl enable pigpiod
```

Descargar el script en la raspberry

```shenable
wget "https://gitlab.com/MaxPow2718r/codigos_servo/-/raw/main/adjval.py"
```

Instalar automaticamente en la Rapsberry:
```sh
sh -c "$(curl -fsSL https://gitlab.com/MaxPow2718r/codigos_servo/-/raw/main/install.sh)"
```

Instalar en el lado del cliente:
```sh
sh -c "$(curl -fsSL https://gitlab.com/MaxPow2718r/codigos_servo/-/raw/main/install_client.sh)"
```

# TODO
- [x] Hacer el script accesible desde otro computador via ssh (sin tener  que hacer
ssh y abrir el terminal).
