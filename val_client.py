#!/usr/bin/python3

import curses
import socket

screen = curses.initscr()
curses.noecho()
curses.cbreak()
screen.keypad(True)

PORT = 12345
HOST = "192.168.0.2"

try:
    while True:
        s = socket.socket()
        s.connect(("127.0.0.1", PORT))

        char = screen.getch()
        if char == ord('q'):
            break

        elif char == curses.KEY_RIGHT:
            command = "incx"
            data = s.send(command.encode())

            val = s.recv(1024).decode("utf-8")

            screen.addstr(0, 0, val + "\n")

        elif char == curses.KEY_LEFT:
            command = "decx"
            data = s.send(command.encode())

            val = s.recv(1024).decode("utf-8")

            screen.addstr(0, 0, val + "\n")

        elif char == curses.KEY_UP:
            command = "incy"
            data = s.send(command.encode())

            val = s.recv(1024).decode("utf-8")

            screen.addstr(0, 0, val + "\n")

        elif char == curses.KEY_DOWN:
            command = "decy"
            data = s.send(command.encode())

            val = s.recv(1024).decode("utf-8")

            screen.addstr(0, 0, val + "\n")

        s.close()
finally:
    curses.nocbreak(); screen.keypad(0); curses.echo()
    curses.endwin()
