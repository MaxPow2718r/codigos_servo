# Instalar librerias
# sudo apt-get install python3-rpi.gpio
# sudo pip3 install gpiozero

from gpiozero import Servo
from time import sleep

# Asignar pin GPIO
servo = Servo(12)

# Empezar en el valor minimo (angulo de 0 grados)
val = -1
# Paso del movimiento
b = 0.1

try:
    while True:
        # Asignar el valor de movimiento al servo
        servo.value = val
        # Tiempo de espera
        sleep(0.5)
        # Cambiar valor
        val = round(val+b, 2)
        if abs(val) == 1:
            b = -1*b
            
except KeyboardInterrupt:
	print("Program stopped")