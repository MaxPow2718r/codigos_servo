script_link="https://gitlab.com/MaxPow2718r/codigos_servo/-/raw/main/adjval.py"
directory=$HOME/.local/bin

mkdir -p $directory

echo "Installing system required dependencies"
sudo apt install pigpio python-pigpio python3-pigpio

echo "Downloading script"
wget -q -O "${directory}/adjval" $script_link

chmod +x "${directory}/adjval"

echo "Setting gpio server"
sudo systemctl enable pigpiod
sudo systemctl start pigpiod
