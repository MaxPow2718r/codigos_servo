#!/usr/bin/python3

import socket

# from gpiozero import AngularServo
# from gpiozero.pins.pigpio import PiGPIOFactory

# factory = PiGPIOFactory()

MAX_ANGLE = 35
MIN_ANGLE = -35
INIT_X = 0
INIT_Y = 0

val_x = INIT_X
val_y = INIT_Y

# Vservo = AngularServo(12, initial_angle=None, pin_factory=factory)
# Hservo = AngularServo(13, initial_angle=None, pin_factory=factory)

s = socket.socket()

port = 12345

s.bind(("", port))

s.listen(5)

try:
    while True:
        c, addr = s.accept()
        data = c.recv(1024).decode("utf-8")

        if data == "incx":
            val_x += 1

            if abs(val_x) >= MAX_ANGLE:
                val_x = (abs(val_x) // val_x) * MAX_ANGLE

        elif data == "decx":
            val_x -= 1

            if abs(val_x) >= MAX_ANGLE:
                val_x = (abs(val_x) // val_x) * MAX_ANGLE

        elif data == "incy":
            val_y += 1

            if abs(val_y) >= MAX_ANGLE:
                val_y = (abs(val_y) // val_y) * MAX_ANGLE

        elif data == "decy":
            val_y -= 1

            if abs(val_y) >= MAX_ANGLE:
                val_y = (abs(val_y) // val_y) * MAX_ANGLE

        # elif data == "zero":
        #     val_x = val_y = 0

        else:
            pass

        val = str(val_x) + " " + str(val_y)
        c.send(val.encode())
        c.close()

        # Hservo.angle = val_x
        # Vservo.angle = val_y

except KeyboardInterrupt:
    c.send("Connection closed".encode())
    c.close()
